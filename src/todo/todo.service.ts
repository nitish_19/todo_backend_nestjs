import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Todo } from './interface/todo.interface';
import { AddTodoDto } from './dto/todo.dto';

@Injectable()
export class TodoService {
  constructor(@InjectModel('Todo') private readonly todoModel: Model<Todo>) {}

  // service for adding a todo API, it is used by the controller which is invoked when
  // their is request on the route is made
  async addTodo(addTodoDto: AddTodoDto): Promise<Todo> {
    const todo = await new this.todoModel(addTodoDto);
    return todo.save();
  }

  // service for retreiving all todos, it is used by the controller, which is also invoked when
  // request on matched route is made through the client side
  async getAllTodos(userEmail: string): Promise<Todo[]> {
    const allTodos: Todo[] = await this.todoModel
      .find({ email: userEmail })
      .exec();
    return allTodos;
  }

  // service updating a todo by its ID, it is used by the controller, which is called when
  // request on matched route is made through the client side
  async updateTodoById(id: string, addTodoDto: AddTodoDto): Promise<Todo> {
    const todo = await this.todoModel.findByIdAndUpdate(id, addTodoDto, {
      new: true,
    });
    return todo;
  }

  // service deleting a todo by its ID, it is used by the controller, which is called when
  // request on matched route is made through the client side
  async deleteTodoById(id: string): Promise<Todo> {
    const todo = await this.todoModel.findByIdAndDelete(id);
    return todo;
  }

  // service deleting all todo, it is used by the controller, which is called when
  // request on matched route is made through the client side
  async deleteAllTodos(): Promise<any> {
    const todo = await this.todoModel.deleteMany();
    return todo;
  }
}
