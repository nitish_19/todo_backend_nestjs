import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { TodoSchema } from './todo.schema';
import { TodoService } from './todo.service';
import { TodoController } from './todo.controller';
import { UsersModule } from 'src/users/users.module';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'Todo', schema: TodoSchema }]),
    UsersModule,
  ],
  exports: [MongooseModule],
  providers: [TodoService],
  controllers: [TodoController],
})
export class TodoModule {}
