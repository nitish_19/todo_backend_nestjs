import * as mongoose from 'mongoose';
const { Schema } = mongoose;

export const TodoSchema = new Schema({
  title: {
    type: String,
    required: true,
  },
  description: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    required: true,
  },
});
