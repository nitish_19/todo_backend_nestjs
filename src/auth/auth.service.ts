import { Injectable, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { LoginUserDto } from '../users/dto/login-user.dto';
import { UsersService } from '../users/users.service';
import { JwtPayload } from './interface/jwt-payload.interface';
import * as bcrypt from 'bcrypt';
import { User } from '../users/interface/users.interface';
import * as dotenv from 'dotenv';

dotenv.config();

@Injectable()
export class AuthService {
  constructor(
    private usersService: UsersService,
    private jwtService: JwtService,
  ) {}

  // validate the login user details using bcrypt to compare the hashed password
  async validateUserByPassword(loginDetails: LoginUserDto) {
    const user = await this.usersService.findOneByEmail(loginDetails.email);
    if (user) {
      const check = bcrypt.compareSync(loginDetails.password, user.password);
      if (check) {
        return this.createJwtPayload(user);
      }
    }
    return null;
  }

  // using users email as a payload to create JWT
  createJwtPayload(user: User) {
    const jwtToken = this.jwtService.sign({ email: user.email });
    return {
      expiresIn: process.env.EXPIRES_IN,
      token: jwtToken,
      email: user.email,
      name: user.name,
    };
  }

  async validateUserByJwt(payload: JwtPayload) {
    const user = await this.usersService.findOneByEmail(payload.email);

    if (user) {
      return true;
    } else {
      throw new UnauthorizedException();
    }
  }
}
