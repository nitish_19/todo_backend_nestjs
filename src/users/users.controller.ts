import {
  Controller,
  Post,
  Body,
  HttpStatus,
  Res,
  Get,
  UseGuards,
  Param,
} from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { User } from './interface/users.interface';
import { UsersService } from './users.service';

@Controller('users')
export class UsersController {
  constructor(private usersService: UsersService) {}

  @Post('signup')
  async create(@Res() res, @Body() createUserDto: CreateUserDto) {
    // Check for email already registered
    const checkUser = await this.usersService.findOneByEmail(
      createUserDto.email,
    );
    if (checkUser) {
      return res.status(HttpStatus.UNPROCESSABLE_ENTITY).json({
        status: 422,
        message: 'User with email already exists',
      });
    }

    const user: User = await this.usersService.create(createUserDto);
    return res.status(HttpStatus.CREATED).json({
      status: 201,
      message: 'success',
      data: user,
    });
  }
}
