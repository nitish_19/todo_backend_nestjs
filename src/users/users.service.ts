import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { User } from './interface/users.interface';
import { CreateUserDto } from './dto/create-user.dto';
import * as bcrypt from 'bcrypt';

@Injectable()
export class UsersService {
  constructor(@InjectModel('Users') private readonly userModel: Model<User>) {}

  // Users creation with password encrypted using brcypt
  async create(createUserDto: CreateUserDto) {
    const salt = bcrypt.genSaltSync(10);
    createUserDto.password = bcrypt.hashSync(createUserDto.password, salt);
    const user = new this.userModel(createUserDto);
    return await user.save();
  }

  // finding users registered with the provided email
  async findOneByEmail(email: string): Promise<User> {
    return await this.userModel.findOne({ email: email });
  }
}
