import * as mongoose from 'mongoose';
const { Schema } = mongoose;

// Users Schema
export const UserSchema = new Schema({
  name: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    unique: true,
    match: /^\S+@\S+\.\S+$/,
    required: true,
  },
  password: {
    type: String,
    required: true,
  },
});
